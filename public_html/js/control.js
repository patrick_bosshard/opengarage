var garage_door_model = {
    door_status: '1',
    success_click: true,
    door_counter: 1
};

var controller = {
    init: function () {
        var me = this;

        $('#garage_ctr').click(function () {
            me.clickRelay();
            if (garage_door_model.success_click) {
                me.readStatus();
                me.updateDoorIcon();
            }
        });

        me.readStatus();
        me.updateDoorIcon();
    },
    updateDoorIcon: function () {
        $('#status').show();
        if (garage_door_model.door_status === '0') {
            $('#garage_img').attr('src', 'img/garage-close.png');
            $('#status').html('Garage schliesst...');
        } else {
            $('#garage_img').attr('src', 'img/garage-open.png');
            $('#status').html('Garage öffnet...');
        }
        $('#status').fadeOut(800);
    },
    readStatus: function () {
        $.ajax({
            // url: 'http://devip/jc',
            url: 'json/ctr_vars.json',
            success: onSuccess,
            error: onError
        });
        function onSuccess(ctr_vars, textStatus, jqXHR) {
            $('#name').text('Name: ' + ctr_vars.name);
            $('#fwv').text('fw_ver: ' + ctr_vars.fwv);

            // $('#rcnt').text(ctr_vars.rcnt);
            $('#rcnt').text('Door counter: ' + (garage_door_model.door_counter++));

            if (garage_door_model.door_status === '1') {
                garage_door_model.door_status = '0';
            } else {
                garage_door_model.door_status = '1';
            }
        }
        function onError(jqXHR, textStatus, errorThrown) {
            alert('Failed to read status.');
        }
    },
    clickRelay: function () {
        $.ajax({
            // url: 'http://devip/cc?dkey=xxx&click=1',
            url: 'json/click_relay.json',
            success: onSuccess,
            error: onError
        });
        function onSuccess(result, textStatus, jqXHR) {
            garage_door_model.success_click = true;
        }
        function onError(jqXHR, textStatus, errorThrown) {
            alert('Failed to click relay.');
            garage_door_model.success_click = false;
        }
    }
};

// make it go!
controller.init();